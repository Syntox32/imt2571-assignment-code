<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
        // Create PDO connection
        $host   = 'localhost';
        $dbname = 'db_assignment1';
        $user   = 'dbtest';
        $passwd = 'dbtest';
        $opts   =  array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

        $this->db = new PDO('mysql:host='.$host.';dbname='.$dbname,
            $user, $passwd, $opts);
		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $set = $this->db->prepare("
            SELECT id, title, author, description
            FROM book
            ORDER BY id");
        $set->execute();

        $result = $set->fetchAll(PDO::FETCH_OBJ);

        return $result;
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        if (!is_numeric($id)) {
            return NULL;
            //throw new PDOException();
        }

        $set = $this->db->prepare("
                SELECT id, title, author, description
                FROM book
                WHERE id=:bookid");
        $set->bindValue(':bookid', $id);
        $set->execute();

        $book = $set->fetch(PDO::FETCH_ASSOC);

        if ($book === FALSE) {
            return NULL;
            //throw new PDOException();
        }

        return new Book(
            $book["title"],
            $book["author"],
            $book["description"],
            $book["id"]);
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	   * @throws PDOException
     */
    public function addBook($book)
    {
        // check for an author
        if ($book->author == NULL || $book->title == NULL) {
            //return NULL;
            throw new PDOException();
        }

        $set = $this->db->prepare("
            INSERT INTO book(title, author, description)
            VALUES(:title, :author, :description)");

        $set->bindValue(':title', $book->title);
        $set->bindValue(':author', $book->author);
        $set->bindValue(':description', $book->description);
        $set->execute();

        $book->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        // check for a valid id, and that author and title is not NULL
        if ($this->getBookById($book->id) == FALSE
            || $book->author == NULL
            || $book->title == NULL) {
              return NULL;
              //throw new PDOException();
            }

        $set = $this->db->prepare("
            UPDATE book
            SET title=:title,author=:author,description=:description
            WHERE id=:id");

        $set->bindValue(':title', $book->title);
        $set->bindValue(':author', $book->author);
        $set->bindValue(':description', $book->description);
        $set->bindValue(':id', $book->id);
        $set->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        // check for a valid id
        if ($this->getBookById($id) == FALSE) {
            return NULL;
        }

        $set = $this->db->prepare("DELETE FROM book WHERE id=:id");

        $set->bindValue(':id', $id);
        $set->execute();
    }
}

?>
